from typing import Type, TypeVar, Dict, Tuple
from pydantic import BaseSettings

C = TypeVar("C", bound="Configuration.Config")


class Configuration(BaseSettings):
    gitlab_api_token: str
    gitlab_renovate_user_name: str

    class Config:
        env_file_encoding = "utf-8"

        @classmethod
        def customise_sources(
            cls: Type[C],
            init_settings: Dict,
            env_settings: Dict,
            file_secret_settings: Dict,
        ) -> Tuple:
            return (env_settings, init_settings, file_secret_settings)
