from typing import List, Optional
import requests
from requests_toolbelt import sessions
from dateutil.parser import parse as date_parse

from patchguard.app.domain.models import Library, PatchRequest, Project
import logging


async def parse_target_lib_from_mr(elem) -> Optional[Library]:
    try:
        split_elem = elem["title"].split()
        return Library(name=split_elem[1], version=split_elem[-1])
    except Exception:
        logging.exception("Could not parser version from title")
        return None


class Gitlab:
    def __init__(self, access_token: str, renovate_user_name: str) -> None:
        self.access_token = access_token
        self.renovate_user_name = renovate_user_name

    async def get_merge_requests(
        self,
        project: Project,
        session: requests.Session = sessions.BaseUrlSession(
            base_url="https://gitlab.com/api/v4/"
        ),
    ) -> List[PatchRequest]:
        # Documentation: https://docs.gitlab.com/ee/api/merge_requests.html
        session.headers["Authorization"] = f"Bearer {self.access_token}"
        response = session.get(
            f"projects/{project.id}/merge_requests?state=all&author_username={self.renovate_user_name}&order_by=created_at&sort=desc&per_page=100"  # noqa: E501
        )

        # TODO: Deal with pagination

        res = []
        for elem in response.json():

            pr = PatchRequest(
                project=project,
                intended_lib=await parse_target_lib_from_mr(elem),
                created=date_parse(elem["created_at"]),
            )

            if elem["merged_at"]:
                pr.completed = date_parse(elem["merged_at"])
            elif elem["closed_at"]:
                pr.closed = date_parse(elem["closed_at"])

            res.append(pr)

        return res
