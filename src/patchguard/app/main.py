from typing import Any, Dict, List
from fastapi import FastAPI

import patchguard
from patchguard.app.adapters.scm_systems import Gitlab
from patchguard.app.config import Configuration
from patchguard.app.domain.models import PatchRequest

app = FastAPI()


@app.get("/health-check")
async def health_check() -> Dict[str, Any]:
    # See: https://tools.ietf.org/id/draft-inadarei-api-health-check-05.html
    return {
        "status": "pass",
        "version": "1",
        "releaseId": str(patchguard.__version__),
        "notes": [""],
        "output": "",
        "checks": {},
    }


@app.get("/")
async def home() -> List[PatchRequest]:
    res = await app.state.gitlab.get_merge_requests()  # TODO
    return res  # type: ignore


@app.on_event("startup")
async def startup() -> None:
    config = Configuration()

    gitlab = Gitlab(
        access_token=config.gitlab_api_token,
        renovate_user_name=config.gitlab_renovate_user_name,
    )
    app.state.gitlab = gitlab
