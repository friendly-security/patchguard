from dataclasses import dataclass
from datetime import datetime
from typing import Literal, Optional

ZULU_STRFTIME_FORMAT = "%Y-%m-%dT%H:%M:%SZ"


@dataclass
class Project:
    id: str
    name: str
    scm: Literal["gitlab", "github"]
    uri: str


@dataclass
class Library:
    name: str
    version: str


@dataclass
class PatchRequest:
    project: Project
    created: datetime
    current_lib: Optional[Library] = None
    intended_lib: Optional[Library] = None
    closed: Optional[datetime] = None
    completed: Optional[datetime] = None

    def is_open(self) -> bool:
        return not self.completed and not self.closed
