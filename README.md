# Patch Guard

Welcome to patch_guard. You can find more extensive documentation over at [readthedocs](https://patchguard.readthedocs.io/en/latest/).

Contributions are welcome. Just get in touch.

## Quickstart

Simply `pip install patchguard` and get going. The cli is available as `patchguard` and
you can run `patchguard --help` to get up to speed on what you can do.

## Development

This project uses `poetry` for dependency management and `pre-commit` for local checks.
